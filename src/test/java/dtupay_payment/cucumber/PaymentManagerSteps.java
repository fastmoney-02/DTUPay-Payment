package dtupay_payment.cucumber;

import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import models.AccountId;
import models.TokenId;
import models.UserId;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.*;

import java.math.BigDecimal;
import java.util.ArrayList;

import dtupay_payment.database.AccountDatabase;
import dtupay_payment.entities.Account;
import dtupay_payment.entities.Token;
import dtupay_payment.entities.Transaction;
import dtupay_payment.exceptions.AccountUnknownException;
import dtupay_payment.exceptions.BankException;
import dtupay_payment.exceptions.InvalidTokenException;
import dtupay_payment.external.interfaces.IBank;
import dtupay_payment.external.interfaces.ITokenService;
import dtupay_payment.managers.AccountManager;
import dtupay_payment.managers.PaymentManager;
import dtupay_payment.managers.TransactionManager;

public class PaymentManagerSteps {
	Account merchant;
	Account customer;
	UserId userId = new UserId("CustomerUser");
	Token token = new Token(new TokenId("Id"), userId);

	PaymentManager paymentManager;
	AccountManager accountManager;
	TransactionManager transactionManager;
	IBank mockBank;
	ITokenService mockTokenService;
	Exception transferException;
	AccountDatabase adb;
	BigDecimal transferAmount;
	
	@Before
	public void setup()	{
		adb = new AccountDatabase();
		transactionManager = new TransactionManager(adb);
		mockBank = mock(IBank.class);
		mockTokenService = mock(ITokenService.class);
		accountManager = new AccountManager(adb);
		paymentManager = new PaymentManager(mockBank, transactionManager, mockTokenService, adb);
	}
	
	@Given("the bank account of a merchant is known")
	public void theBankAccountOfAMerchantIsKnown() {
		merchant = new Account(new UserId("MerchantUser"), new AccountId("MerchantAccount"));
		assertTrue(accountManager.registerAccount(merchant.getUserId(), merchant.getAccountId()));
	}

	@Given("the bank account of a customer is known")
	public void theBankAccountOfACustomerIsKnown() {
		customer = new Account(userId, new AccountId("CustomerAccount"));
		assertTrue(accountManager.registerAccount(customer.getUserId(), customer.getAccountId()));
	}

	@Given("a valid token of the customer is used")
	public void aValidTokenOfTheCustomerIsUsed() {
		when(mockTokenService.getSingleToken(token.getId())).thenReturn(token);
		SetTokenValid(true);
	}

	@When("a {int} credit payment is made by the  merchant using the token")
	public void aCreditPaymentIsMadeByTheMerchantUsingTheToken(Integer int1) {
	    try {
	    	transferAmount = new BigDecimal(int1);
			paymentManager.payMerchant(token.getId(), merchant.getUserId(), transferAmount);
		} catch (BankException | InvalidTokenException | AccountUnknownException e ) {
			transferException = e;
		}
	}

	@Then("the bank is invoked")
	public void theBankIsInvoked() throws BankException {
		verify(mockBank, times(1)).transferFromTo(customer.getAccountId(), merchant.getAccountId(), transferAmount, token.getId().getValue());
	}

	@Then("a transaction is registered")
	public void aTransactionIsRegistered() {
		ArrayList<Transaction> transactions = transactionManager.getCustomerTransaction(customer.getUserId());
		assertEquals(1, transactions.size());
		assertTransaction(merchant.getUserId(), transferAmount, token, true, transactions.get(0));
	}
	
	@Then("a failed transaction is registered")
	public void aFailedTransactionIsRegistered() {
		ArrayList<Transaction> transactions = transactionManager.getCustomerTransaction(customer.getUserId());
		assertEquals(1, transactions.size());
		assertTransaction(merchant.getUserId(), transferAmount, token, false, transactions.get(0));
	}


	@Given("an invalid token of the customer is used")
	public void anInvalidTokenOfTheCustomerIsUsed() {
		when(mockTokenService.getSingleToken(token.getId())).thenReturn(token);
		SetTokenValid(false);
	}

	@Then("the bank is not invoked")
	public void theBankIsNotInvoked() throws BankException {
		verify(mockBank, never()).transferFromTo(any(), any(), any(), any());
	}

	@Then("the token is reported as invalid")
	public void theTokenIsReportedAsInvalid() {
		assertNotNull(transferException);
		assertEquals(InvalidTokenException.class, transferException.getClass());
	}

	@Given("a non-existent token is used")
	public void aNonExistentTokenIsUsed() {
		when(mockTokenService.getSingleToken(token.getId())).thenReturn(null);
	}


	@Then("no transaction is registered")
	public void noTransactionIsRegistered() {
		ArrayList<Transaction> transactions = transactionManager.getCustomerTransaction(merchant.getUserId());
		assertEquals(0, transactions.size());
	}

	@Then("the token is reported as not found")
	public void theTokenIsReportedAsNotFound() {
	    assertNotNull(transferException);
	    assertEquals(InvalidTokenException.class, transferException.getClass());
	    assertEquals("Token did not exist", transferException.getMessage());
	}

	@Given("the transaction will be rejected by the bank")
	public void theTransactionWillBeRejectedByTheBank() throws BankException {
		doThrow(mock(BankException.class)).when(mockBank).transferFromTo(any(), any(), any(), any());
	}

	@Then("the payment is reported as failed")
	public void thePaymentIsReportedAsFailed() {
		assertNotNull(transferException);
		// Need to use instanceof because we are throwing a mock exception
		assertTrue("The thrown exception was not a bank exception", transferException instanceof BankException );
	}
	
	@Given("the token of a user not known to the service")
	public void theTokenOfAUserNotKnownToTheService() {
		when(mockTokenService.getSingleToken(token.getId())).thenReturn(token);
		SetTokenValid(true);
	}

	@Then("and the user is reported as not known")
	public void andTheUserIsReportedAsNotKnown() {
	    assertNotNull(transferException);
	    assertEquals(AccountUnknownException.class, transferException.getClass());
	}
	

	@Given("{int} transactions has been made to the merchant")
	public void transactionsHasBeenMadeToTheMerchant(Integer int1) {
		for (int i = 0; i < int1; i++) {
			transactionManager.createTransaction(new Transaction(merchant.getUserId(), new BigDecimal(0), token, true));	
		}
	}
	
	ArrayList<Transaction> transactions;
	
	@When("the merchant retrieves his transactions")
	public void theMerchantRetrievesHisTransactions() {
		transactions = transactionManager.getMerchantTransaction(merchant.getUserId());
	}
	
	@Then("{int} transactions are returned")
	public void transactionsAreReturned(Integer int1) {
	    assertEquals((int)int1, transactions.size());
	}


	@When("the customer account is deleted")
	public void theCustomerAccountIsDeleted() {
	    accountManager.deleteAccount(customer.getUserId());
	}
	
	@Then("the customer account is no longer stored")
	public void theCustomerAccountIsNoLongerStored() {
		assertNull(adb.getAccount(customer.getUserId()));
	}
	
	void assertTransaction(UserId merchant, BigDecimal amount, Token usedToken, boolean success, Transaction actual)
	{
		assertEquals(merchant, actual.getMerchantId());
		assertEquals(amount, actual.getAmount());
		assertEquals(usedToken, actual.getToken());
		assertEquals(success, actual.getSuccess());
	}
	
	void SetTokenValid(boolean valid)
	{
		when(mockTokenService.consume(token.getId())).thenReturn(valid);
	}
	
}
