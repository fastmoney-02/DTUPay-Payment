package dtupay_payment.entities;

import models.AccountId;
import models.UserId;

/**
 * The Class Account.
 */
public class Account {
	
	/**
	 * Instantiates a new account.
	 *
	 * @param userId the user id
	 * @param accountId the account id
	 */
	public Account(UserId userId, AccountId accountId) {
		super();
		this.accountId = accountId;
		this.userId = userId;
	}
	
	/**
	 * Gets the account id.
	 *
	 * @return the account id
	 */
	public AccountId getAccountId() {
		return accountId;
	}
	
	/**
	 * Gets the user id.
	 *
	 * @return the user id
	 */
	public UserId getUserId() {
		return userId;
	}
	
	/** The account id. */
	private AccountId accountId;
	
	/** The user id. */
	private UserId userId;
}
