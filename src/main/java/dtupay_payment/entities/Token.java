package dtupay_payment.entities;

import dataTransferObject.TokenDTO;
import models.TokenId;
import models.UserId;

/**
 * The Class Token.
 */
public class Token {
	
	/** The id. */
	private TokenId id;
	
	/** The user id. */
	private UserId userId;
	
	/**
	 * Instantiates a new token.
	 */
	public Token() {}
	
	/**
	 * Instantiates a new token.
	 *
	 * @param tokenId the token id
	 * @param userId the user id
	 */
	public Token(TokenId tokenId, UserId userId)
	{
		this.id = tokenId;
		this.userId = userId;
	}
	
	/**
	 * Instantiates a new token.
	 *
	 * @param token the token
	 */
	public Token(TokenDTO token)
	{
		this.id = new TokenId(token.getId());
		this.userId = new UserId(token.getUserId());
	}
	
	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public TokenId getId()
	{
		return id;
	}
	
	/**
	 * Gets the user id.
	 *
	 * @return the user id
	 */
	public UserId getUserId()
	{
		return userId;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if(obj == this)
			return true;
		
		if(obj instanceof Token)
		{
			return ((Token)obj).id == id;
		}
		return false;
	}
}
