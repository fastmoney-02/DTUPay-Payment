package dtupay_payment.entities;
import java.math.BigDecimal;

import models.UserId;

/**
 * The Class Transaction.
 */
public class Transaction {

	/** The merchant id. */
	UserId merchantId;
	
	/** The amount. */
	BigDecimal amount;
	
	/** The token. */
	Token token;
	
	/** The success. */
	boolean success;
	
	/**
	 * Instantiates a new transaction.
	 */
	public Transaction() {}
	
	/**
	 * Instantiates a new transaction.
	 *
	 * @param merchantId the merchant id
	 * @param amount the amount
	 * @param token the token
	 * @param success the success
	 */
	public Transaction(UserId merchantId, BigDecimal amount, Token token, boolean success) {
		this.merchantId = merchantId;
		this.amount = amount;
		this.token = token;
		this.success = success;
	}



	/**
	 * Gets the merchant id.
	 *
	 * @return the merchant id
	 */
	public UserId getMerchantId() {
		return merchantId;
	}

	/**
	 * Gets the amount.
	 *
	 * @return the amount
	 */
	public BigDecimal getAmount() {
		return amount;
	}

	/**
	 * Gets the token.
	 *
	 * @return the token
	 */
	public Token getToken() {
		return token;
	}
	
	/**
	 * Gets the success.
	 *
	 * @return the success
	 */
	public boolean getSuccess(){
		return success;
	}
}

