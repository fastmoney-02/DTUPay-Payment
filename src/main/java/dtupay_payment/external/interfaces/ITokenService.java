package dtupay_payment.external.interfaces;

import dataTransferObject.RequestTokensArgs;
import dtupay_payment.entities.Token;
import models.TokenId;

/**
 * The Interface ITokenService.
 */
public interface ITokenService {
	
	/**
	 * Request tokens.
	 *
	 * @param args requestTokenArgs, userid and amount
	 * @return the string[] of token ids
	 * @throws Exception the exception
	 */
	public String[] requestTokens(RequestTokensArgs args) throws Exception;
	
	/**
	 * Gets the single token.
	 *
	 * @param tokenId if of the token
	 * @return the the found token
	 */
	public Token getSingleToken(TokenId tokenId);
	
	/**
	 * Consume.
	 *
	 * @param tokenId of the token
	 * @return true, if successful
	 */
	boolean consume(TokenId tokenId);
	
	
}
