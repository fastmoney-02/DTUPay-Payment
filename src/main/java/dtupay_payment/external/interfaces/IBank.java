package dtupay_payment.external.interfaces;

import java.math.BigDecimal;

import dtupay_payment.exceptions.BankException;
import models.AccountId;

/**
 * The Interface IBank.
 */
public interface IBank {
	
	/**
	 * Transfer from to.
	 *
	 * @param debitorId of the customer
	 * @param creditorId of the merchant
	 * @param amount of money
	 * @param description of the transaction
	 * @throws BankException the bank exception
	 */
	public void transferFromTo(AccountId debitorId, AccountId creditorId, BigDecimal amount, String description) throws BankException;
	
	/**
	 * Retire account.
	 *
	 * @param accountId of the account to retire
	 * @throws BankException the bank exception
	 */
	public void retireAccount(AccountId accountId) throws BankException;
}
