package dtupay_payment.external.connectors;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import dataTransferObject.CustomerDTO;
import dtupay_payment.managers.IAccountListener;
import models.AccountId;
import models.UserId;

/**
 * The Class CustomerEventHandler.
 */
public class CustomerEventHandler implements IRabbitMQEventHandler {
	
	/** The object mapper. */
	private ObjectMapper objectMapper = new ObjectMapper();
	
	/** The account listener. */
	IAccountListener accountListener;
	
	/**
	 * Instantiates a new customer event handler.
	 *
	 * @param accountListener the account listener
	 */
	public CustomerEventHandler(IAccountListener accountListener) {
		this.accountListener = accountListener;
	}
	
	/* (non-Javadoc)
	 * @see dtupay_payment.external.connectors.IRabbitMQEventHandler#HandleMessage(java.lang.String, java.lang.String)
	 */
	@Override
	public void HandleMessage(String routingKey, String content) {
		if(routingKey.endsWith("Created"))
		{
			OnCustomerCreated(content);
		}
		else if(routingKey.endsWith("Deleted"))
		{
			OnCustomerDeleted(content);
		}
	}
	
	/**
	 * On customer deleted.
	 *
	 * @param content the content
	 */
	private void OnCustomerDeleted(String content)
	{
		try {
			String userId = objectMapper.readValue(content, String.class);
			accountListener.deleteAccount(new UserId(userId));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * On customer created.
	 *
	 * @param content the content
	 */
	private void OnCustomerCreated(String content)
	{
		CustomerDTO customerDTO;
		try {
			customerDTO = objectMapper.readValue(content, CustomerDTO.class);
			accountListener.registerAccount(new UserId(customerDTO.getId()), new AccountId(customerDTO.getAccountId()));
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
