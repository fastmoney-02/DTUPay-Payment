package dtupay_payment.external.connectors;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import dataTransferObject.MerchantDTO;
import dtupay_payment.managers.IAccountListener;
import models.AccountId;
import models.UserId;

/**
 * The Class MerchantEventHandler.
 */
public class MerchantEventHandler implements IRabbitMQEventHandler{
	
	/** The object mapper. */
	private ObjectMapper objectMapper = new ObjectMapper();
	
	/** The account listener. */
	IAccountListener accountListener;
	
	/**
	 * Instantiates a new merchant event handler.
	 *
	 * @param accountListener the account listener
	 */
	public MerchantEventHandler(IAccountListener accountListener) {
		this.accountListener = accountListener;
	}
	
	
	/* (non-Javadoc)
	 * @see dtupay_payment.external.connectors.IRabbitMQEventHandler#HandleMessage(java.lang.String, java.lang.String)
	 */
	@Override
	public void HandleMessage(String routingKey, String content) {
		if(routingKey.endsWith("Created"))
		{
			OnMerchantCreated(content);
		}
		else if(routingKey.endsWith("Deleted"))
		{
			OnMerchantDeleted(content);
		}
	}

	/**
	 * On merchant created.
	 *
	 * @param content the content
	 */
	private void OnMerchantCreated(String content) {
		MerchantDTO merchantDTO;
		try {
			merchantDTO = objectMapper.readValue(content, MerchantDTO.class);
			accountListener.registerAccount(new UserId(merchantDTO.getId()), new AccountId(merchantDTO.getAccountId()));
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * On merchant deleted.
	 *
	 * @param content the content
	 */
	private void OnMerchantDeleted(String content) {
		try {
			String userId = objectMapper.readValue(content, String.class);
			accountListener.deleteAccount(new UserId(userId));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
