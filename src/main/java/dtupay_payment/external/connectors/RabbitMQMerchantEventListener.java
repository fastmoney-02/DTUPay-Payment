package dtupay_payment.external.connectors;

import java.io.IOException;
import java.net.URISyntaxException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.concurrent.TimeoutException;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.DeliverCallback;

/**
 * The listener interface for receiving rabbitMQMerchantEvent events.
 * The class that is interested in processing a rabbitMQMerchantEvent
 * event implements this interface, and the object created
 * with that class is registered with a component using the
 * component's <code>addRabbitMQMerchantEventListener<code> method. When
 * the rabbitMQMerchantEvent event occurs, that object's appropriate
 * method is invoked.
 *
 * @see RabbitMQMerchantEventEvent
 */
public class RabbitMQMerchantEventListener {
	
	/** The Constant EXCHANGE_NAME. */
	private static final String EXCHANGE_NAME = "events";
	
	/** The account listener. */
	private IRabbitMQEventHandler accountListener;
	
	/** The Constant AMQP_URL. */
	private static final String AMQP_URL = "amqp://guest:guest@fastmoney-02.compute.dtu.dk:5672";
	
	/** The event channel. */
	private Channel eventChannel;
	
	/**
	 * Instantiates a new rabbit MQ merchant event listener.
	 *
	 * @param accountListener the account listener
	 */
	public RabbitMQMerchantEventListener(IRabbitMQEventHandler accountListener) 
	{
		this.accountListener = accountListener;
	}
	
	/**
	 * Gets the exchange.
	 *
	 * @return the channel
	 * @throws KeyManagementException the key management exception
	 * @throws NoSuchAlgorithmException the no such algorithm exception
	 * @throws URISyntaxException the URI syntax exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws TimeoutException the timeout exception
	 */
	private static Channel GetExchange() throws KeyManagementException, NoSuchAlgorithmException, URISyntaxException, IOException, TimeoutException 
	{
		ConnectionFactory factory = new ConnectionFactory();
		factory.setUri(AMQP_URL);
		Connection connection = factory.newConnection();
		Channel channel = connection.createChannel();
		channel.exchangeDeclare(EXCHANGE_NAME, "topic");
		return channel;
	}
	
	/**
	 * Attach listener.
	 *
	 * @throws Exception the exception
	 */
	public void attachListener() throws Exception {
		eventChannel = GetExchange();
		String queueName = "PaymentMerchantEventQueue";	
		eventChannel.queueDeclare(queueName, true, false, false, null);
		eventChannel.queueBind(queueName, EXCHANGE_NAME, "Merchant.*");

		DeliverCallback deliverCallback = (consumerTag, delivery) -> {
			String message = new String(delivery.getBody(), "UTF-8");
			System.out.println("[x] receiving " + message);
			
			accountListener.HandleMessage(delivery.getEnvelope().getRoutingKey(), message);
		};
		eventChannel.basicConsume(queueName, true, deliverCallback, consumerTag -> {
				// Empty
		});
		
		
	}
	
	
}
