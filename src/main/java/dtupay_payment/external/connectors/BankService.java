package dtupay_payment.external.connectors;

import java.math.BigDecimal;

import dtu.ws.fastmoney.*;
import dtupay_payment.exceptions.BankException;
import dtupay_payment.external.interfaces.IBank;
import models.AccountId;

/**
 * The Class BankService.
 */
public class BankService implements IBank{

	/** The bank service. */
	dtu.ws.fastmoney.BankService bankService; 
	
	/**
	 * Instantiates a new bank service.
	 */
	public BankService() {
		bankService = new BankServiceService().getBankServicePort();
	}
	
	/* (non-Javadoc)
	 * @see dtupay_payment.external.interfaces.IBank#transferFromTo(models.AccountId, models.AccountId, java.math.BigDecimal, java.lang.String)
	 */
	@Override
	public void  transferFromTo(AccountId debitorId, AccountId creditorId, BigDecimal amount, String description) throws BankException{
		try {
			bankService.transferMoneyFromTo(debitorId.toString(), creditorId.toString(), amount, description);
		} catch (Exception e) {
			throw new BankException(e.getMessage());
		}
	}

	/* (non-Javadoc)
	 * @see dtupay_payment.external.interfaces.IBank#retireAccount(models.AccountId)
	 */
	@Override
	public void retireAccount(AccountId accountId) throws BankException {
		try {
			bankService.retireAccount(accountId.toString());
		} catch (Exception e) {
			throw new BankException(e.getMessage());
		}
	}

}
