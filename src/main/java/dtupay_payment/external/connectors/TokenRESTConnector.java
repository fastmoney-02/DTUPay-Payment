package dtupay_payment.external.connectors;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.Response.Status.Family;

import dataTransferObject.RequestTokensArgs;
import dataTransferObject.TokenDTO;
import dtupay_payment.entities.Token;
import dtupay_payment.external.interfaces.ITokenService;
import models.TokenId;

/**
 * The Class TokenRESTConnector.
 */
public class TokenRESTConnector implements ITokenService {

	/** The c. */
	Client c = ClientBuilder.newClient();
	
	/** The target. */
	WebTarget target;
	
	/**
	 * Instantiates a new token REST connector.
	 *
	 * @param target the target
	 */
	public TokenRESTConnector(String target) {
		super();
		this.target = c.target(target);
	}
	
	/* (non-Javadoc)
	 * @see dtupay_payment.external.interfaces.ITokenService#consume(models.TokenId)
	 */
	@Override
	public boolean consume(TokenId tokenId) {
		boolean result = target
		.path("token")
		.path(tokenId.toString())
		.path("consume")
		.request(MediaType.TEXT_PLAIN)
		.put(Entity.text(""), Boolean.class);
		return result;
	}

	/* (non-Javadoc)
	 * @see dtupay_payment.external.interfaces.ITokenService#requestTokens(dataTransferObject.RequestTokensArgs)
	 */
	@Override
	public String[] requestTokens(RequestTokensArgs args) throws Exception {
		Response response = target
				.path("token")
				.request()
				.post(Entity.json(args));
		
		if(response.getStatusInfo().getFamily().equals(Status.Family.SERVER_ERROR)) {
			return new String[0];
		}
		return response.readEntity(String[].class);
	}

	/* (non-Javadoc)
	 * @see dtupay_payment.external.interfaces.ITokenService#getSingleToken(models.TokenId)
	 */
	@Override
	public Token getSingleToken(TokenId tokenId) {
		Response response = target
				.path("token")
				.path(tokenId.getValue())
				.request(MediaType.APPLICATION_JSON)
				.get();
		if(response.getStatusInfo().getFamily() == Family.SUCCESSFUL)
			return new Token(response.readEntity(TokenDTO.class));
		return null;
	}

}
