package dtupay_payment.external.connectors;

/**
 * The Interface IRabbitMQEventHandler.
 */
public interface IRabbitMQEventHandler {
	
	/**
	 * Handle message.
	 *
	 * @param routingKey the routing key
	 * @param content the content
	 */
	void HandleMessage(String routingKey, String content);
}
