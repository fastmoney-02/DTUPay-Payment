package dtupay_payment;

import java.math.BigDecimal;

import dtupay_payment.exceptions.AccountUnknownException;
import dtupay_payment.exceptions.BankException;
import dtupay_payment.exceptions.InvalidTokenException;
import models.TokenId;
import models.UserId;

/**
 * The Interface IPaymentService.
 */
public interface IPaymentService {

	/**
	 * Pay merchant.
	 *
	 * @param tokenId id of the token to be used
	 * @param merchantId the merchant id to receive money
	 * @param amount of money
	 * @return true, if successful
	 * @throws BankException the bank exception
	 * @throws InvalidTokenException the invalid token exception
	 * @throws AccountUnknownException the account unknown exception
	 */
	boolean payMerchant(TokenId tokenId, UserId merchantId, BigDecimal amount) throws BankException, InvalidTokenException, AccountUnknownException;

}