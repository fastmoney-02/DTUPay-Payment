package dtupay_payment.managers;

import java.math.BigDecimal;

import dtupay_payment.IPaymentService;
import dtupay_payment.database.IAccountRepository;
import dtupay_payment.entities.Account;
import dtupay_payment.entities.Token;
import dtupay_payment.entities.Transaction;
import dtupay_payment.exceptions.AccountUnknownException;
import dtupay_payment.exceptions.BankException;
import dtupay_payment.exceptions.InvalidTokenException;
import dtupay_payment.external.interfaces.IBank;
import dtupay_payment.external.interfaces.ITokenService;
import models.*;

/**
 * The Class PaymentManager.
 */
public class PaymentManager implements IPaymentService {

	/** The bank. */
	IBank bank;
	
	/** The transaction manager. */
	TransactionManager transactionManager;
	
	/** The token service. */
	ITokenService tokenService;
	
	/** The account database. */
	IAccountRepository accountDatabase;

	/**
	 * Instantiates a new payment manager.
	 *
	 * @param bank the bank
	 * @param transactionManager the transaction manager
	 * @param tokenService the token service
	 * @param accountDatabase the account database
	 */
	public PaymentManager(IBank bank, TransactionManager transactionManager, ITokenService tokenService, IAccountRepository accountDatabase) {
		this.bank = bank;
		this.tokenService = tokenService;
		this.transactionManager = transactionManager;
		this.accountDatabase = accountDatabase;
	}

	/* (non-Javadoc)
	 * @see dtupay_payment.IPaymentService#payMerchant(models.TokenId, models.UserId, java.math.BigDecimal)
	 */
	public boolean payMerchant(TokenId tokenId, UserId merchantId, BigDecimal amount) throws BankException, InvalidTokenException, AccountUnknownException {
		Token token = tokenService.getSingleToken(tokenId);
		if(token == null) {
			throw new InvalidTokenException("Token did not exist");
		}
		
		Account customer = accountDatabase.getAccount(token.getUserId());
		if(customer == null)
		{
			throw new AccountUnknownException("The customer is not known");
		}
		Account merchant = accountDatabase.getAccount(merchantId);
		
		boolean consumed = tokenService.consume(tokenId);
		if(consumed) {
			return makeTransfer(merchantId, amount, token, customer, merchant);
		}
		else
		{
			logFailedTransaction(merchantId, amount, token);
			throw new InvalidTokenException("Token was invalid");
		}
	}

	private void logFailedTransaction(UserId merchantId, BigDecimal amount, Token token)
	{
		transactionManager.createTransaction(new Transaction(merchantId, amount, token, false));	
	}
	
	private void logSuccessfulTransaction(UserId merchantId, BigDecimal amount, Token token)
	{
		transactionManager.createTransaction(new Transaction(merchantId, amount, token, true));	
	}
	
	/**
	 * Instantiates a new payment manager.
	 *
	 * @param merchantId the id
	 * @param amount the transfer amount
	 * @param token the authorization token
	 * @param merchant the merchant to receive payment
	 */
	private boolean makeTransfer(UserId merchantId, BigDecimal amount, Token token, Account customer, Account merchant)
			throws BankException {
		try {
			bank.transferFromTo(customer.getAccountId(), merchant.getAccountId(), amount, token.getId().getValue());
			logSuccessfulTransaction(merchantId, amount, token);
			return true;	
		} catch (BankException e) {
			logFailedTransaction(merchantId, amount, token);
			throw e;
		}
	}
	
}
