package dtupay_payment.managers;

import java.util.ArrayList;
import java.util.stream.Collectors;

import dtupay_payment.database.ITransactionRepository;
import dtupay_payment.entities.Transaction;
import models.UserId;

/**
 * The Class TransactionManager.
 */
public class TransactionManager {
	
	/** The transaction repository. */
	ITransactionRepository transactionRepository;
	
	/**
	 * Instantiates a new transaction manager.
	 *
	 * @param transactionRepository the transaction repository
	 */
	public TransactionManager(ITransactionRepository transactionRepository){
		this.transactionRepository = transactionRepository;
	}
	
	/**
	 * Gets a customers transactions.
	 *
	 * @param userId of the customer
	 * @return the customers transactions
	 */
	public ArrayList<Transaction> getCustomerTransaction(UserId userId){
		ArrayList<Transaction> transactions = transactionRepository.getTransaction();
		transactions = (ArrayList<Transaction>) transactions.stream().filter(t -> t.getToken().getUserId().equals(userId)).collect(Collectors.toList());
		return transactions;
	}
	
	/**
	 * Gets a merchants transactions.
	 *
	 * @param userId merchant id
	 * @return the merchants transactions
	 */
	public ArrayList<Transaction> getMerchantTransaction(UserId userId){
		ArrayList<Transaction> transactions = transactionRepository.getTransaction();
		transactions = (ArrayList<Transaction>) transactions.stream().filter(t -> t.getMerchantId().equals(userId)).collect(Collectors.toList());
		return transactions;
	}
	
	/**
	 * Creates a transaction.
	 *
	 * @param transaction to be created
	 */
	public void createTransaction(Transaction transaction){
		transactionRepository.createTransaction(transaction);
	}
	

}
