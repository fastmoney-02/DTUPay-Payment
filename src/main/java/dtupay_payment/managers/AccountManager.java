package dtupay_payment.managers;

import dtupay_payment.database.IAccountRepository;
import dtupay_payment.entities.Account;
import models.AccountId;
import models.UserId;

/**
 * The Class AccountManager.
 */
public class AccountManager implements IAccountListener {

	/** The database. */
	IAccountRepository database;
	
	/**
	 * Instantiates a new account manager.
	 *
	 * @param database the database
	 */
	public AccountManager(IAccountRepository database) {
		this.database = database;
	}
	
	/* (non-Javadoc)
	 * @see dtupay_payment.managers.IAccountListener#registerAccount(models.UserId, models.AccountId)
	 */
	@Override
	public boolean registerAccount(UserId id, AccountId accountId) {
		Account account = new Account(id, accountId);

		return database.createAccount(account);
	}
	
	/* (non-Javadoc)
	 * @see dtupay_payment.managers.IAccountListener#deleteAccount(models.UserId)
	 */
	@Override
	public boolean deleteAccount(UserId userId) {
		return database.deleteAccount(userId);
	}
}
