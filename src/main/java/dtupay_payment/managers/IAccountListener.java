package dtupay_payment.managers;

import models.AccountId;
import models.UserId;

/**
 * The listener interface for receiving IAccount events.
 * The class that is interested in processing a IAccount
 * event implements this interface, and the object created
 * with that class is registered with a component using the
 * component's <code>addIAccountListener<code> method. When
 * the IAccount event occurs, that object's appropriate
 * method is invoked.
 *
 * @see IAccountEvent
 */
public interface IAccountListener {
	
	/**
	 * Register account.
	 *
	 * @param id of the user
	 * @param accountId of the account
	 * @return true, if successful
	 */
	public boolean registerAccount(UserId id, AccountId accountId);
	
	/**
	 * Delete account.
	 *
	 * @param userId to be deleted
	 * @return true, if successful
	 */
	public boolean deleteAccount(UserId userId);
}