package dtupay_payment.exceptions;

/**
 * The Class InvalidTokenException.
 */
@SuppressWarnings("serial")
public class InvalidTokenException extends Exception {
	
	/**
	 * Instantiates a new invalid token exception.
	 *
	 * @param message the message
	 */
	public InvalidTokenException(String message) {
		super(message);
	}
}
