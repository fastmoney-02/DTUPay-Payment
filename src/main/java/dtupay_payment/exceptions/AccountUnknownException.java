package dtupay_payment.exceptions;

/**
 * The Class AccountUnknownException.
 */
@SuppressWarnings("serial")
public class AccountUnknownException extends Exception {
	
	/**
	 * Instantiates a new account unknown exception.
	 *
	 * @param message the message
	 */
	public AccountUnknownException(String message) {
		super(message);
	}
}
