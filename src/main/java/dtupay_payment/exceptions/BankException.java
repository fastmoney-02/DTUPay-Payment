package dtupay_payment.exceptions;

/**
 * The Class BankException.
 */
@SuppressWarnings("serial")
public class BankException extends Exception {
	
	/**
	 * Instantiates a new bank exception.
	 *
	 * @param message the message
	 */
	public BankException(String message)
	{
		super(message);
	}
}
