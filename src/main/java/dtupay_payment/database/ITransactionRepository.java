/*
 * 
 */
package dtupay_payment.database;

import java.util.ArrayList;

import dtupay_payment.entities.Transaction;

/**
 * The Interface ITransactionRepository.
 */
public interface ITransactionRepository {
	
	/**
	 * Gets all transactions.
	 *
	 * @return the transaction
	 */
	public ArrayList<Transaction> getTransaction();
	
	/**
	 * Creates a transaction.
	 *
	 * @param transaction to be created
	 */
	public void createTransaction(Transaction transaction);
	
	
}
