package dtupay_payment.database;

import java.util.ArrayList;
import java.util.List;

import dtupay_payment.entities.Account;
import dtupay_payment.entities.Transaction;
import models.AccountId;
import models.UserId;

/**
 * The Class AccountDatabase.
 */
public class AccountDatabase implements IAccountRepository, ITransactionRepository {
	
	/** The store. */
	List<Account> store = new ArrayList<Account>();
	
	/** The transactions. */
	ArrayList<Transaction> transactions = new ArrayList<Transaction>();
	
	/* (non-Javadoc)
	 * @see dtupay_payment.database.IAccountRepository#createAccount(dtupay_payment.entities.Account)
	 */
	@Override
	public boolean createAccount(Account account) {
		return store.add(account);
	}

	/* (non-Javadoc)
	 * @see dtupay_payment.database.IAccountRepository#getAccount(models.UserId)
	 */
	@Override
	public Account getAccount(UserId user) {
		return store.stream().filter(a -> a.getUserId().equals(user)).findFirst().orElse(null);
	}

	/* (non-Javadoc)
	 * @see dtupay_payment.database.ITransactionRepository#getTransaction()
	 */
	@Override
	public ArrayList<Transaction> getTransaction() {
		return transactions;
	}

	/* (non-Javadoc)
	 * @see dtupay_payment.database.ITransactionRepository#createTransaction(dtupay_payment.entities.Transaction)
	 */
	@Override
	public void createTransaction(Transaction transaction) {
		transactions.add(transaction);
		
	}

	/* (non-Javadoc)
	 * @see dtupay_payment.database.IAccountRepository#deleteAccount(models.UserId)
	 */
	@Override
	public boolean deleteAccount(UserId userId) {
		Account result = store.stream().filter(a -> a.getUserId().equals(userId)).findFirst().orElse(null);
		if(result == null) {
			return false;
		}
		return store.remove(result);
	}
}
