package dtupay_payment.database;

import dtupay_payment.entities.Account;
import models.AccountId;
import models.UserId;

/**
 * The Interface IAccountRepository.
 */
public interface IAccountRepository {
	
	/**
	 * Gets the account.
	 *
	 * @param userId of the account
	 * @return the found account
	 */
	Account getAccount(UserId userId);
	
	/**
	 * Creates the account.
	 *
	 * @param account to be created
	 * @return true, if successful
	 */
	boolean createAccount(Account account);
	
	/**
	 * Delete account.
	 *
	 * @param userId of the account to be deleted
	 * @return true, if successful
	 */
	boolean deleteAccount(UserId userId);
}
