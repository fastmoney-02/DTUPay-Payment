package dtupay_payment.endpoints;

import java.util.ArrayList;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import dataTransferObject.TransactionDTO;
import dtupay_payment.entities.Transaction;
import dtupay_payment.managers.TransactionManager;
import models.UserId;

/**
 * The Class TransactionHistoryEndpoint.
 */
@Path("/transactions")
public class TransactionHistoryEndpoint {
	
	/** The transaction manager. */
	TransactionManager transactionManager;
	
	/**
	 * Instantiates a new transaction history endpoint.
	 *
	 * @param application the application
	 */
	public TransactionHistoryEndpoint(@Context Application application) {
		transactionManager = ((RestApplication)application).getTransactionManager();
	}
	
	/**
	 * Gets the customer transaction history.
	 *
	 * @param customerId the customer id
	 * @return the customer transaction history
	 */
	@GET
	@Path("/customer/{customerId}")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response getCustomerTransactionHistory(@PathParam("customerId") String customerId) {
		ArrayList<Transaction> transactions = transactionManager.getCustomerTransaction(new UserId(customerId));
		TransactionDTO[] transactionDTOs = new TransactionDTO[transactions.size()];
		for (int i = 0; i < transactionDTOs.length; i++) {
			TransactionDTO temp = new TransactionDTO(transactions.get(i).getMerchantId().getValue(), 
					transactions.get(i).getAmount(), 
					transactions.get(i).getToken().getId().getValue(), 
					transactions.get(i).getSuccess());
			transactionDTOs[i] = temp;
		}
		return Response.ok().entity(transactionDTOs).build();
	}
	
	/**
	 * Gets the merchant transaction history.
	 *
	 * @param merchantId the merchant id
	 * @return the merchant transaction history
	 */
	@GET
	@Path("/merchant/{merchantId}")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response getMerchantTransactionHistory(@PathParam("merchantId") String merchantId) {
		ArrayList<Transaction> transactions = transactionManager.getMerchantTransaction(new UserId(merchantId));
		TransactionDTO[] transactionDTOs = new TransactionDTO[transactions.size()];
		for (int i = 0; i < transactionDTOs.length; i++) {
			TransactionDTO temp = new TransactionDTO(transactions.get(i).getMerchantId().getValue(), 
					transactions.get(i).getAmount(), 
					transactions.get(i).getToken().getId().getValue(), 
					transactions.get(i).getSuccess());
			transactionDTOs[i] = temp;
		}
		return Response.ok().entity(transactionDTOs).build();
	}
}
