package dtupay_payment.endpoints;

import javax.ws.rs.Consumes;
import javax.ws.rs.InternalServerErrorException;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import dataTransferObject.PaymentArgs;
import dtupay_payment.exceptions.AccountUnknownException;
import dtupay_payment.exceptions.BankException;
import dtupay_payment.exceptions.InvalidTokenException;
import dtupay_payment.managers.PaymentManager;
import io.undertow.util.BadRequestException;
import models.TokenId;
import models.UserId;

/**
 * The Class PaymentEndpoint.
 */
@Path("/payments")
public class PaymentEndpoint {
	
	/** The payment manager. */
	PaymentManager paymentManager;
	
	/**
	 * Instantiates a new payment endpoint.
	 *
	 * @param application the application
	 */
	public PaymentEndpoint(@Context Application application) {
		paymentManager = ((RestApplication)application).getPaymentManager();
	}
	
	
	/**
	 * Payment.
	 *
	 * @param args the args
	 * @return true, if successful
	 * @throws BadRequestException the bad request exception
	 */
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	public boolean payment(PaymentArgs args) throws BadRequestException {
		System.out.println(args.getTokenId());
		try {
			if(paymentManager.payMerchant(
					new TokenId(args.getTokenId()),
					new UserId(args.getMerchantId()),
					args.getAmount()))
				return true;
			throw new BadRequestException("Payment failed");
		} catch (BankException e) {
			throw new InternalServerErrorException("Payment failed at bank: \n" + e.getMessage());
		}
		catch (InvalidTokenException e) {
			throw new BadRequestException("Supplied token was invalid: \n" + e.getMessage());
		} catch (AccountUnknownException e) {
			throw new InternalServerErrorException("Account for token not found: \n" + e.getMessage());
		}
	}
}
