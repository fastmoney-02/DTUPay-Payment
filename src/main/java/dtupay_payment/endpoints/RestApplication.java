package dtupay_payment.endpoints;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

import dtupay_payment.database.AccountDatabase;
import dtupay_payment.external.connectors.BankService;
import dtupay_payment.external.connectors.CustomerEventHandler;
import dtupay_payment.external.connectors.MerchantEventHandler;
import dtupay_payment.external.connectors.RabbitMQCustomerEventListener;
import dtupay_payment.external.connectors.RabbitMQMerchantEventListener;
import dtupay_payment.external.connectors.TokenRESTConnector;
import dtupay_payment.external.interfaces.IBank;
import dtupay_payment.external.interfaces.ITokenService;
import dtupay_payment.managers.AccountManager;
import dtupay_payment.managers.PaymentManager;
import dtupay_payment.managers.TransactionManager;

/**
 * The Class RestApplication.
 */
@ApplicationPath("/")
public class RestApplication extends Application {
	
	/** The database. */
	private AccountDatabase database = new AccountDatabase();
	
	/** The account manager. */
	private AccountManager accountManager = new AccountManager(database);
	
	/** The transaction manager. */
	private TransactionManager transactionManager = new TransactionManager(database);
	
	/** The token service. */
	private ITokenService tokenService = new TokenRESTConnector("http://dtupay-tokenservice:8080/");
	
	/** The bank component. */
	private IBank bankComponent = new BankService();
	
	/** The payment manager. */
	private PaymentManager paymentManager = new PaymentManager(bankComponent, transactionManager, tokenService, database);
	
	/** The customer event interceptor. */
	RabbitMQCustomerEventListener customerEventInterceptor;
	
	/** The merchant event interceptor. */
	RabbitMQMerchantEventListener merchantEventInterceptor;
	
	
	/**
	 * Instantiates a new rest application.
	 */
	public RestApplication() {
		InterceptCustomerEvents();
		InterceptMerchantEvents();
	}
	
	/**
	 * Intercept customer events.
	 */
	private void InterceptCustomerEvents()
	{
		try {
			customerEventInterceptor = new RabbitMQCustomerEventListener(new CustomerEventHandler(accountManager));
			customerEventInterceptor.attachListener();
		} catch (Exception e) {
			throw new Error("Failed to attach to rabbitMQ", e);
		}
	}
	
	/**
	 * Intercept merchant events.
	 */
	private void InterceptMerchantEvents()
	{
		try {
			merchantEventInterceptor = new RabbitMQMerchantEventListener(new MerchantEventHandler(accountManager));
			merchantEventInterceptor.attachListener();
		} catch (Exception e) {
			throw new Error("Failed to attach to rabbitMQ", e);
		}
	}

	/**
	 * Gets the account manager.
	 *
	 * @return the account manager
	 */
	public AccountManager getAccountManager() {
		return accountManager;
	}

	/**
	 * Gets the payment manager.
	 *
	 * @return the payment manager
	 */
	public PaymentManager getPaymentManager() {
		return paymentManager;
	}
	
	/**
	 * Gets the transaction manager.
	 *
	 * @return the transaction manager
	 */
	public TransactionManager getTransactionManager() {
		return transactionManager;
	}
}
