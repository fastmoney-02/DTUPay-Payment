FROM adoptopenjdk:8-jre-hotspot
WORKDIR /usr/src
COPY target/DTUPay-PaymentService-thorntail.jar /usr/src
CMD java -Xms256m -Xmx512m \
	-Djava.net.preferIPv4Stack=true \
	-Djava.net.preferIPv4Addresses=true \
	-jar DTUPay-PaymentService-thorntail.jar
