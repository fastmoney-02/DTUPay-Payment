Feature: PaymentManager

Scenario: Sucessful payment
	Given the bank account of a merchant is known
	And the bank account of a customer is known
	And a valid token of the customer is used
	When a 10 credit payment is made by the  merchant using the token 
	Then the bank is invoked 
    And a transaction is registered

Scenario: Invalid Token
	Given the bank account of a merchant is known
	And the bank account of a customer is known
	And an invalid token of the customer is used
	When a 10 credit payment is made by the  merchant using the token 
    Then the bank is not invoked
    And a failed transaction is registered
    And the token is reported as invalid 

Scenario: Non-existent Token
	Given the bank account of a merchant is known
	And a non-existent token is used
	When a 10 credit payment is made by the  merchant using the token 
    Then the bank is not invoked
    And no transaction is registered
    And the token is reported as not found

Scenario: The transaction is rejected by the bank
	Given the bank account of a merchant is known
	And the bank account of a customer is known
	And a valid token of the customer is used
    And the transaction will be rejected by the bank
	When a 10 credit payment is made by the  merchant using the token 
    Then the bank is invoked
    And  a failed transaction is registered
    And  the payment is reported as failed  

Scenario: Unknown customer
	Given the bank account of a merchant is known
	And the token of a user not known to the service
	When a 10 credit payment is made by the  merchant using the token 
	Then the bank is not invoked
	And no transaction is registered
	And and the user is reported as not known

Scenario: Merchant Transactions
	Given the bank account of a merchant is known
	And 3 transactions has been made to the merchant
	When the merchant retrieves his transactions
	Then 3 transactions are returned
	
Scenario: Delete Customer Account
	Given the bank account of a customer is known
	When the customer account is deleted
	Then the customer account is no longer stored